let $wrapCard = document.querySelector('.wrap-card');
let $cardColors = document.querySelectorAll('.card-options');

$wrapCard.addEventListener('click', function(event) {

    let $this = event.target;
    console.log($this);
    let $card = $this.parentNode.parentNode.parentNode;
    let $cardContent = $card.querySelector('.card-content');

    if ($this.dataset.color) {
        $card.dataset.color = $this.dataset.color;

        for (let position = 0; position < $cardColors.length; position++) {
            $cardColors[position].classList.remove('isActive');
        }

        $this.classList.add('isActive');

    };
    if ($this.classList.contains('card_delete')) {
        $card.remove();
    };

    if ($this.classList.contains('card_edit')) {
        if ($cardContent.getAttribute('contenteditable') == 'false') {
            $cardContent.setAttribute('contenteditable', 'true');
            $cardContent.focus();
            $this.classList.add('isActive');
        } else {
            $cardContent.setAttribute('contenteditable', 'false');
            $cardContent.blur();
            $this.classList.remove('isActive');
        }
    };
});



